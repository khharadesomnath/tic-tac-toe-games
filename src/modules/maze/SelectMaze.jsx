import * as React from "react";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import "../../assets/scss/Maze.scss";
import { Button } from "@mui/material";
import { Component } from "react";
import Maze3x3 from "./Maze3x3";
import Maze4x4 from "./Maze4x4";
import Maze5x5 from "./Maze5x5";

export class SelectMaze extends Component {
  constructor(props) {
    super(props);
    this.state = {
      maze: "3",
      start: false,
      userChoose: "x",
      computerChoose: "o",
    };
  }
  changeUserValue = (e) => {
    if (e.target.value == "x") {
      this.setState({
        userChoose: "x",
        computerChoose: "o",
      });
    } else {
      this.setState({
        userChoose: "o",
        computerChoose: "x",
      });
    }
  };
  homeRedirect = () => {
    this.setState({
      maze: "3",
      start: false,
      userChoose: "x",
      computerChoose: "o",
    });
  };
  render() {
    let _this = this;
    return (
      <React.Fragment>
        <div className="main-section">
          <div className="maze-heading">
            <h1>Tic-Tac-Toe</h1>
          </div>

          {this.state.start ? (
            this.state.maze === "3" ? (
              <div>
                <Maze3x3
                  userValue={this.state.userChoose}
                  computerValue={this.state.computerChoose}
                  homeRedirect={this.homeRedirect}
                  _this={_this}
                />
              </div>
            ) : this.state.maze === "4" ? (
              <div>
                <Maze4x4
                  userValue={this.state.userChoose}
                  computerValue={this.state.computerChoose}
                    homeRedirect={this.homeRedirect}
                    _this={_this}
                />
              </div>
            ) : this.state.maze === "5" ? (
              <div>
                <Maze5x5
                  userValue={this.state.userChoose}
                  computerValue={this.state.computerChoose}
                      homeRedirect={this.homeRedirect}
                      _this={_this}
                />
              </div>
            ) : null
          ) : (
            <div>
              <div className="maze-section">
                <div className="maze-box">
                  <div className="maze-select">
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Select Maze</FormLabel>
                      <RadioGroup
                        aria-label="gender"
                        defaultValue="3"
                        name="radio-buttons-group"
                        onChange={(e) => {
                          this.setState({
                            maze: e.target.value,
                          });
                        }}
                      >
                        <FormControlLabel
                          className="maze-label"
                          value="3"
                          control={<Radio />}
                          label="3x3"
                        />
                        <FormControlLabel
                          className="maze-label"
                          value="4"
                          control={<Radio />}
                          label="4x4"
                        />
                        <FormControlLabel
                          className="maze-label"
                          value="5"
                          control={<Radio />}
                          label="5x5"
                        />
                      </RadioGroup>
                    </FormControl>
                  </div>
                  <div className="maze-select">
                    <FormControl component="fieldset">
                      <FormLabel component="legend">Select x or o</FormLabel>
                      <RadioGroup
                        aria-label="gender"
                        defaultValue="x"
                        name="radio-buttons-group"
                        onChange={this.changeUserValue}
                      >
                        <FormControlLabel
                          value="x"
                          className="maze-label"
                          control={<Radio />}
                          label="x"
                        />
                        <FormControlLabel
                          className="maze-label"
                          value="o"
                          control={<Radio />}
                          label="o"
                        />
                      </RadioGroup>
                    </FormControl>
                  </div>
                </div>
              </div>
              <div class="d-flex justify-content-center">
                <Button
                  className="game-btn"
                  onClick={() => {
                    this.setState({
                      start: true,
                    });
                  }}
                >
                  Start Game
                </Button>
              </div>
            </div>
          )}

          {}
        </div>
      </React.Fragment>
    );
  }
}

export default SelectMaze;
