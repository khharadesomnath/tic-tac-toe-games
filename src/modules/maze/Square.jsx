function Square(props) {
  return (
    <div className={"square"} {...props}>
      {console.log(props)}
      {props.x ? (
        <span className="user-block">{props.userValue}</span>
      ) : props.o ? (
        <span className="cmp-block">{props.computerValue}</span>
      ) : null}
    </div>
  );
}

export default Square;
