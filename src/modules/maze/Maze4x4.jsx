import "../../App.css";
import Board from "./Board4";
import * as React from "react";
import Square from "./Square";
import { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import Countdown from "react-countdown";
const defaultSquares = () => new Array(16).fill(null);

const lines = [
  [0, 1, 2, 3],
  [4, 5, 6, 7],
  [8, 9, 10, 11],
  [12, 13, 14, 15],
  [0, 4, 8, 12],
  [1, 5, 9, 13],
  [2, 6, 10, 14],
  [3, 7, 11, 15],
  [0, 5, 10, 15],
  [3, 6, 9, 12],
];
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};
function Maze4(props) {
  const [squares, setSquares] = useState(defaultSquares());
  const [winner, setWinner] = useState(null);
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));
  const [counter, setCounter] = useState(60000);
  const [drawMatch, setDrawMatch] = useState(false)

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    const isComputerTurn =
      squares.filter((square) => square !== null).length % 2 === 1;
    const linesThatAre = (a, b, c, d) => {
      return lines.filter((squareIndexes) => {
        const squareValues = squareIndexes.map((index) => squares[index]);
        return (
          JSON.stringify([a, b, c, d].sort()) ===
          JSON.stringify(squareValues.sort())
        );
      });
    };
    const emptyIndexes = squares
      .map((square, index) => (square === null ? index : null))
      .filter((val) => val !== null);
    const playerWon = linesThatAre("x", "x", "x", "x").length > 0;
    const computerWon = linesThatAre("o", "o", "o", "o").length > 0;
    if (playerWon) {
      setWinner("x");
      handleClickOpen();
    }
    if (computerWon) {
      setWinner("o");
      handleClickOpen();
    }

    const putComputerAt = (index) => {
      let newSquares = squares;
      newSquares[index] = "o";
      setSquares([...newSquares]);
    };
    if (isComputerTurn) {
      const winingLines = linesThatAre("o", "o", null);
      if (winingLines.length > 0) {
        const winIndex = winingLines[0].filter(
          (index) => squares[index] === null
        )[0];
        putComputerAt(winIndex);
        return;
      }

      const linesToBlock = linesThatAre("x", "x", null);
      if (linesToBlock.length > 0) {
        const blockIndex = linesToBlock[0].filter(
          (index) => squares[index] === null
        )[0];
        putComputerAt(blockIndex);
        return;
      }

      const linesToContinue = linesThatAre("o", null, null);
      if (linesToContinue.length > 0) {
        putComputerAt(
          linesToContinue[0].filter((index) => squares[index] === null)[0]
        );
        return;
      }

      const randomIndex =
        emptyIndexes[Math.ceil(Math.random() * emptyIndexes.length)];
      putComputerAt(randomIndex);
    }
  }, [squares]);
  function handleSquareClick(index) {
    if (squares.filter(x => { return x === null }).length <= 2) {
      handleClickOpen();
      setDrawMatch(true)
    } else {
      setCounter(60000);
      const isPlayerTurn =
        squares.filter((square) => square !== null).length % 2 === 0;
      if (isPlayerTurn) {
        let newSquares = squares;
        newSquares[index] = "x";
        setSquares([...newSquares]);
      }
    }

  }

  return (
    <div>
      <div className="counter count3">
        Time :
        <Countdown
          date={Date.now() + counter}
          renderer={({ hours, minutes, seconds, completed }) => {
            if (completed) {
              setTimeout(() => {
                props._this.setState({
                  maze: "3",
                  start: false,
                  userChoose: "x",
                  computerChoose: "o",
                })
              }, 100)
              return "Expired"
            } else {
              // Render a countdown
              return <span>{` ${seconds} seconds`}</span>;
            }
          }}
        ></Countdown>
      </div>
      <main>
        <Board>
          {squares.map((square, index) => (
            <Square
              x={square === "x" ? 1 : 0}
              o={square === "o" ? 1 : 0}
              onClick={() => handleSquareClick(index)}
              userValue={props.userValue}
              computerValue={props.computerValue}
            />
          ))}
        </Board>
        {!!winner && winner === "x" && (
          <Dialog
            fullScreen={fullScreen}
            open={open}
            className="won-dialog"
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
          >
            <DialogContent>
              <DialogContentText className="won-text">
                Congratulations, You Won!!!
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={props.homeRedirect}>
                Exit
              </Button>
              <Button onClick={props.homeRedirect} autoFocus>
                Rematch
              </Button>
            </DialogActions>
          </Dialog>
        )}
        {drawMatch ? (
          <Dialog
            className="lose-dialog"
            fullScreen={fullScreen}
            open={open}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
          >
            <DialogContent>
              <DialogContentText className="lose-text">
                draw Match!!!
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={props.homeRedirect}>
                Exit
              </Button>
              <Button onClick={props.homeRedirect} autoFocus>
                Rematch
              </Button>
            </DialogActions>
          </Dialog>
        ) : null}
        {!!winner && winner === "o" && (
          // <div className="result red">You LOST!</div>
          <Dialog
            className="lose-dialog"
            fullScreen={fullScreen}
            open={open}
            onClose={handleClose}
            aria-labelledby="responsive-dialog-title"
          >
            <DialogContent>
              <DialogContentText className="lose-text">
                You Did Well, Better Luck Next Time!!!
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={props.homeRedirect}>
                Exit
              </Button>
              <Button onClick={props.homeRedirect} autoFocus>
                Rematch
              </Button>
            </DialogActions>
          </Dialog>
        )}
      </main>
    </div>
  );
}

export default Maze4;
