import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import landingRoutes from "../../routes/App";
export class AppPage extends Component {
  render() {
    return (
      <div>
        <div>
          <Switch>
            {landingRoutes.map((prop, key) => {
              return (
                <Route path={prop.path} component={prop.component} key={key} />
              );
            })}
          </Switch>
        </div>
      </div>
    );
  }
}

export default AppPage;
